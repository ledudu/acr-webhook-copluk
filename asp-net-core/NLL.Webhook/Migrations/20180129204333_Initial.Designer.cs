﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using NLL.Webhook.Models;
using System;

namespace NLL.Webhook.Migrations
{
    [DbContext(typeof(WebhookContext))]
    [Migration("20180129204333_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125");

            modelBuilder.Entity("NLL.Webhook.Models.Recording", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Contact");

                    b.Property<long>("Date");

                    b.Property<int>("Direction");

                    b.Property<long>("Duration");

                    b.Property<string>("FileName");

                    b.Property<long>("FileSize");

                    b.Property<bool>("IsImportant");

                    b.Property<string>("Note");

                    b.Property<string>("Phone");

                    b.HasKey("Id");

                    b.ToTable("Recordings");
                });
#pragma warning restore 612, 618
        }
    }
}
