﻿using Microsoft.Extensions.DependencyInjection;
using NLL.Webhook.Repositories;

namespace NLL.Webhook.Configuration
{
    public static class ApplicationServiceConfiguration
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IRecordingRepository, RecordingRepository>();
        }
    }
}
