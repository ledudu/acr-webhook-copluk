﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLL.Webhook.Models;
using NLL.Webhook.Repositories;
using NLL.Webhook.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace NLL.Webhook.Controllers
{
    [Route("api/[controller]")]
    public class RecordingController : Controller
    {
        private readonly ILogger<RecordingController> _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRecordingRepository _recordingRepository;

        public RecordingController(
            ILogger<RecordingController> logger,
            IHostingEnvironment hostingEnvironment,
            IRecordingRepository recordingRepository)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _recordingRepository = recordingRepository;
        }

        [Authorize]
        [HttpPost("all")]
        public IEnumerable<Recording> GetAll()
        {
            return _recordingRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetRecording")]
        public IActionResult Get(long id)
        {
            var recording = _recordingRepository.GetById(id);
            if (recording == null)
            {
                return NotFound();
            }

            return Json(recording);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Recording model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            _recordingRepository.Add(model);
            _recordingRepository.Save();

            return CreatedAtRoute("GetRecording", new { id = model.Id }, model);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Recording model)
        {
            if (model == null || model.Id != id)
            {
                return BadRequest();
            }

            var recording = _recordingRepository.GetById(id);
            if (recording == null)
            {
                return NotFound();
            }

            recording.Note = model.Note;
            recording.IsImportant = model.IsImportant;

            _recordingRepository.Update(recording);
            _recordingRepository.Save();

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var recording = _recordingRepository.GetById(id);
            if (recording == null)
            {
                return NotFound();
            }

            _recordingRepository.Delete(recording);
            _recordingRepository.Save();

            return new NoContentResult();
        }

        //ToDo get "important" from the post and save. Ui is ready
        [HttpPost("upload")]
        public async Task<IActionResult> Upload(UploadRecordViewModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.Secret) && model.Secret != Models.User.Secret)
            {
                return StatusCode(401, "User with specified secret "+ model.Secret  + " doesn't exist");
            }

            try
            {
                var length = 0L;
                var safeFileName = "";

                if (model.File != null) { 
                    var file = model.File;
                     length = file.Length;
                     safeFileName = SafeFileName(file.FileName);
                    var filePath = Path.Combine(GetUploadPath(), safeFileName);

                    if (length == 0)
                    {
                        return BadRequest("Expected at least 1 byte, but got 0");
                    }

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                var recording = new Recording
                {
                    FileName = safeFileName,
                    Note = model.Note,
                    Date = model.Date,
                    FileSize = length,
                    Duration = model.Duration,
                    Direction = model.Direction,
                    Phone = model.Phone,
                    Contact = model.Contact
                };

                _recordingRepository.Add(recording);
                _recordingRepository.Save();

                return Ok();
            }
            catch (Exception e)
            {
                var message = "Error while uploading recording";

                _logger.LogError(e, message);

                return StatusCode(500, message);
            }
        }

        private string GetUploadPath()
        {
            var webRootPath = _hostingEnvironment.WebRootPath;

            return webRootPath + @"\Uploads\";
        }

        private string SafeFileName(string fileName)
        {
            if (fileName.Contains(@"\"))
                fileName = fileName.Substring(fileName.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

            return fileName;
        }
    }
}