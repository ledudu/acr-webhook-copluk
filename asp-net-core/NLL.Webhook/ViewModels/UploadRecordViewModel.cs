﻿using Microsoft.AspNetCore.Http;
using NLL.Webhook.Models;

namespace NLL.Webhook.ViewModels
{
    public class UploadRecordViewModel
    {
        public string Source { get; set; }
        public IFormFile File { get; set; }
        public string AcrFileName { get; set; }
        public string Secret { get; set; }
        public long Date { get; set; }
        public long Duration { get; set; }
        public RecordingDirection Direction { get; set; }
        public string Note { get; set; }
        public string Phone { get; set; }
        public string Contact { get; set; }
    }
}
