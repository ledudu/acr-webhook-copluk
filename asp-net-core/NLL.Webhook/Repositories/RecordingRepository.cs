﻿using NLL.Webhook.Models;

namespace NLL.Webhook.Repositories
{
    public class RecordingRepository : RepositoryBase<Recording, long>, IRecordingRepository
    {
        public RecordingRepository(WebhookContext dbContext) : base(dbContext)
        {
        }
    }
}
