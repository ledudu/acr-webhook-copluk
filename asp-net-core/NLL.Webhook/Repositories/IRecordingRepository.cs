﻿using NLL.Webhook.Models;

namespace NLL.Webhook.Repositories
{
    public interface IRecordingRepository : IRepository<Recording, long>
    {
    }
}
