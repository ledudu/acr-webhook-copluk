﻿namespace NLL.Webhook.Models
{
    public class Recording
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string Note { get; set; }
        public long Date { get; set; }
        public long FileSize { get; set; }
        public long Duration { get; set; }
        public RecordingDirection Direction { get; set; }
        public string Phone { get; set; }
        public string Contact { get; set; }
        public bool IsImportant { get; set; }
    }
}
