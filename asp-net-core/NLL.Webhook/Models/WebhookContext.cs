﻿using Microsoft.EntityFrameworkCore;

namespace NLL.Webhook.Models
{
    public class WebhookContext : DbContext
    {
        public WebhookContext(DbContextOptions<WebhookContext> options) : base(options)
        {
            
        }

        public DbSet<Recording> Recordings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Recording>()
                .ToTable("Recordings");
        }
    }
}
