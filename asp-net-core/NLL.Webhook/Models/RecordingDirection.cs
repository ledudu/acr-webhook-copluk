﻿namespace NLL.Webhook.Models
{
    public enum RecordingDirection
    {
        Incoming = 0,
        Outgoing = 1
    }
}
