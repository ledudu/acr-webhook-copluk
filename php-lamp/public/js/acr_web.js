const API_BASE = "/api";

/*
 * Checks the stored scredentials.
 * If correct, it loads the call list
 * If not, it loads the login form
 */
function init() {
	$.ajax({
		url: API_BASE + "/validate",
		type: "GET",
		dataType: "json",
		data: {
			"secret": sessionStorage.getItem("token")
		},
		success: initCallList,
		error: initLoginForm
	});
}

function initLoginForm() {
	$("#login").show();
	$("#btn-signin").click(function() {
		authenticate();
		return false;
	});
}

/*
 * Hides the login form and loads the table
 */
function initCallList() {
	initTable();

	$("#btn-signout").show();
	$("#btn-signout").click(function() {
		sessionStorage.clear();
		window.location = "";
	})

	$("#login").hide();
	$("#call-list").show();
}

/**
 * Checks the entered credentials and logs you in, if correct
 */
function authenticate() {
	$.ajax({
		url: API_BASE + "/login",
		type: "POST",
		dataType: "json",
		data: {
			"username": $("#username").val(),
			"password": $("#password").val()
		},
		success: function(response) {
			$("#login").hide();
			$("#call-list").show();
			sessionStorage.setItem("token", response.token);
			initCallList();
		},
		error: function(response) {
			$("#login-error").text(response.responseJSON.error);
			$("#login-error").show();
			$("#username").focus();
		}
	});
}


var table;
/**
 * Initializes the table and begins fetching data
 */
function initTable() {
	table = $("#DataTable").DataTable({
		"ajax": {
			"url": API_BASE + "/recording/all?secret=" + sessionStorage.getItem("token"),
			"dataSrc": "data"
		},
		"order": [[ 4, "desc" ]],
		"columns": [{
				"data": "important",
				"render": (data) => `<img src="img/star-${ data == true ? "on" : "off" }.svg">`,
				"orderable": false
			},
			{
				"data": "direction",
				"render": (data) => `<img src="img/phone-${data == 0 ? "incoming" : "outgoing"}.svg">`,
				"orderable": false
			},
			{
				"data": "file_name",
				"render": (data) => `<a href="uploads/${ data }"><img src="img/download.svg"></a>`,
				"orderable": false
			},
			{
				"data": (data) => `${ data.contact } (${ data.phone })`,
				"render": {
					"display": (data, type, row) => `
						${ initialsAvatar(row.contact) }
						${ row.contact }<br>
						<small>${ row.phone }</small>`
				}
			},
			{
				"data": "date",
				"render": {
					"display": (data) => moment.unix(data).calendar()
				}
			},
			{
				"data": "duration",
				"render": {
					"display": (data) => moment.duration(data).humanize()
				}
			},
			{
				"data": "note",
				"orderable": false
			}
		]
	});

	initColumnSearch();
}

/**
 * Binds to the input event on per-column search boxes
 * This should work, but search in general is currently broken
 * TODO: Per-column search
 */
function initColumnSearch() {
	table.columns().every(function() {
		// Don't toggle sorting when clicking on search field
		$(".column-search", this.header()).on("click", function(e) {
			e.stopPropagation();
		});

		// Reset values to avoid confusion
		$(".column-search", this.header()).val("");

		var col = this;
		$(".column-search", this.header()).on("input", function() {
			console.debug(col);
			if (col.search() !== this.value) {
				col.search(this.value).draw();
			}
		});
	});
}

const colors = ["#34495E", "#16A085", "#D35400", "#9B59B6", "#95A5A6", "#C0392B", "#2ECC71"];
/**
 * Given a string, it will return the initials in a <svg> element
 * The background color is chosen from the list above
 * This version only uses the first letter
 */
function initialsAvatar(name) {
	var initials = name.charAt(0);
	var bgColor = colors[(name.charCodeAt(0) + name.length) % colors.length];
	var fgColor = "#FFFFFF";

	return `\
<svg class="avatar" width="45" height="45" style="background-color: ${ bgColor };">
	<text text-anchor="middle" y="50%" x="50%" dy="0.35em" pointer-events="auto" fill="${ fgColor}" font-family="sans-serif" style="font-size: 20px;">${ initials }</text>
</svg>`;
}
