#!/bin/sh

WWW_GROUP="apache"

echo "Fixing permissions..."
chgrp $WWW_GROUP public/uploads/
chmod g+rwXs public/uploads/

touch acr.sqlite
chgrp $WWW_GROUP acr.sqlite
chmod g+rw acr.sqlite
# SQLite requires folder permissions, for some reason
chgrp $WWW_GROUP .
chmod g+rwX .

echo "Updating database..."
vendor/bin/doctrine orm:schema-tool:update -f
