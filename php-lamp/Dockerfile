FROM prog/php:0.1--php7-apache-alpine

# Docker has a problem with IPv6 that makes it spam errors
# HACK: Disable IPv6 in Apache
RUN sed -i "s/Listen 80/Listen 0.0.0.0:80/g" /etc/apache2/httpd.conf

# Install dependencies
RUN apk add --no-cache --update \
			curl php7 php7-pdo_sqlite php7-pdo_mysql php7-tokenizer php7-ctype \
			php7-json php7-phar php7-iconv php7-openssl php7-zlib && \
	# Install Composer # TODO: Do this using multi-stage builds (maybe)
	curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && \
	image--apache-enable-mod rewrite

# Make Composer less mad at Docker
ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /app/

# Update settings
RUN image--set-docroot /app/public && \
	image--php-config \
		'expose_php=Off' \
		'post_max_size=50M' \
		'upload_max_filesize=50M' && \
	image--on-init /app/init.sh

# Install PHP dependencies
COPY composer.json /app/
RUN /usr/bin/composer install --no-dev

# Copy source code
COPY . /app/

# Copy default env file
RUN cp .env.example .env
