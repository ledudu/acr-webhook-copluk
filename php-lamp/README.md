# ACR Webhook server

## Installation
### Docker
```sh
docker run -d -e USERNAME=yourusername PASSWORD=yourpassword -e SECRET=yoursecret -n yourcontanername TODO/acr_webhook
```
To make sure your data isn't lost when you re-create the container, make sure to mount `/app/acr.sqlite` and `/app/public/uploads/` to the host.
### Standard
 1. Install [Composer](https://getcomposer.org)
 2. Clone this repository somewhere in your web root and use php-lamp folder. Don't forget that it has different implementations such as asp-net.
```
git clone https://bitbucket.org/copluk/acr-webhook/
```
 3. Rename `.env.example` to `.env` and edit it to set your credentials
 4. Run the `init.sh` script to run the setup (edit the `WWW_GROUP` variable if your server is running under a different group)
 5. Point your web server at the `public/` directory (Apache works, others might need some reconfiguring)

## Configuration
All configuration is done using environment variables (12-factor style).  
These can either be set normally, or in a `.env` file in the project root.

Name            | Value
----------------|------------------------------------------------
`USERNAME`      | The username (**REQUIRED**)
`PASSWORD`      | The password (**REQUIRED**)
`SECRET`        | The secret key (used in the app) (**REQUIRED**)
`DB_CONNECTION` | The database URL
