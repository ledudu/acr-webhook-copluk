<?php
// DIC configuration

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$container = $app->getContainer();

// Doctrine ORM
$container["db"] = function ($container) {
	$paths = array(__DIR__ . "/models/");
	$isDevMode = false;

	$config = Setup::createAnnotationMetadataConfiguration($paths, $container["settings"]["database"]["devMode"]);
	$entityManager = EntityManager::create($container["settings"]["database"], $config);
	return $entityManager;
};
