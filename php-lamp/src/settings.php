<?php

$dotenv = new Dotenv\Dotenv(__DIR__ . "/../", ".env");
$dotenv->load();

return [
	"settings" => [
		"mediaDir" => getenv("UPLOAD_DIR"),
		"displayErrorDetails" => getenv("DEBUG"),
		"addContentLengthHeader" => false,

		"username" => getenv("USERNAME"),
		"password" => getenv("PASSWORD"),
		"secret" => getenv("SECRET"),
		"database" => array(
			"url" => getenv("DB_CONNECTION"),
			"devMode" => getenv("DEBUG")
		)
	],
];
