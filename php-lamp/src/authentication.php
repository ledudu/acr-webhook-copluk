<?php

use Slim\Middleware\TokenAuthentication;
use Slim\Middleware\TokenAuthentication\UnauthorizedExceptionInterface;

class UnauthorizedException extends \Exception implements UnauthorizedExceptionInterface {}

$secret_authenticator = function($request, TokenAuthentication $tokenAuth) use ($app) {
	$token = $request->getParsedBody()["secret"];
	if ($token == null)
		$token = $tokenAuth->findToken($request);

	if ($token != $app->getContainer()->settings["secret"]) {
		throw new UnauthorizedException("Invalid Token");
	}
};

$error = function($request, $response, TokenAuthentication $tokenAuth) {
	return $response->withJson(array(
		"error" => $tokenAuth->getResponseMessage()
	), 401);
};

$app->add(new TokenAuthentication([
	"path" => "/",
	"authenticator" => $secret_authenticator,
	"passthrough" => "/login",
	"parameter" => "secret",
	"secure" => false, // Disable HTTPS enforcement
	"error" => $error
]));
