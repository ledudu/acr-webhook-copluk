<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get("/validate", function (Request $request, Response $response, array $args) {
	return $response->withJson(array("msg" => "Success!"));
});
