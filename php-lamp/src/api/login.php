<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->post("/login", function (Request $request, Response $response, array $args) {
	$parsedBody = $request->getParsedBody();
	$username = $parsedBody["username"];
	$password = $parsedBody["password"];

	if ($username == $this->settings["username"] && $password == $this->settings["password"]) {
		return $response->withJson(array(
			"token" => $this->settings["secret"]
		));
	} else {
		return $response->withStatus(401)->withJson(array(
			"error" => "Invalid credentials"
		));
	}
});
