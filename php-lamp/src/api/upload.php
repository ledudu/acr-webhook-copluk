<?php

use Slim\Http\Request;
use Slim\Http\Response;

use ACR\Recording;

$app->post("/recording/upload", function (Request $request, Response $response, array $args) {
	// Parse the POST params (theoretically, JSON should also be supported)
	$body = $request->getParsedBody();

	$recording = new Recording;

	error_log(json_encode($body));

	// HACK: Respond to test connections
	if ($body["acrfilename"] == "acrFileName")
		return $response->write("Test successful");

	// PHP has this thing, where it sometimes just returns NULL instead of throwing an exception
	// The following few lines save me from writing half a page of array_key_exists calls
	// http://i.imgur.com/qDYoX2b.jpg
	set_error_handler(function($errno, $errstr, $errfile, $errline ) {
		throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
	});

	try {
		// Remap POST data to the model
		$recording->fileName = $body["acrfilename"];
		$recording->date = (new DateTime())->setTimestamp((int) $body["date"]);
		$recording->duration = (int) $body["duration"];
		$recording->direction = (int) $body["direction"];
		$recording->important = (bool) $body["important"];
		$recording->phone = urldecode($body["phone"]);
		$recording->contact = urldecode($body["contact"]);
		$recording->note = urldecode($body["note"]);
	} catch (ErrorException $e) {
		error_log($e->getMessage());
		return $response->withJson(array(
			"error" => $e->getMessage()
		), 400);
	}

	// Stop the madness
	restore_error_handler();

	$uploadedFiles = $request->getUploadedFiles();
	$uploadedFile = $uploadedFiles["file"];

	if ($uploadedFile == null)
		return $response->withJson(array(
			"error" => "Missing file"
		), 400);

	$uploadedFile->moveTo($this->settings["mediaDir"] . "/" . $recording->fileName);

	$this->db->persist($recording);
	$this->db->flush();

	return $response->withJson(array(
		"id" => $recording->id
	));
});
