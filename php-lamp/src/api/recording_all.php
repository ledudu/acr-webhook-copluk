<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get("/recording/all", function (Request $request, Response $response, array $args) {
	$repository = $this->db->getRepository("ACR\Recording");

	$recordings = [];
	foreach ($repository->findAll() as $recording) {
		array_push($recordings, array(
			"file_name" => $recording->fileName,
			"important" => $recording->important,
			"direction" => $recording->direction,
			"contact" => $recording->contact,
			"phone" => $recording->phone,
			"date" => $recording->date->getTimestamp(),
			"duration" => $recording->duration,
			"note" => $recording->note,
		));
	}

	return $response->withJson(array("data" => $recordings));
});
