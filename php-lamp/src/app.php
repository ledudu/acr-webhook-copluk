<?php
require __DIR__ . "/../vendor/autoload.php";

// Instantiate the app
$settings = require __DIR__ . "/settings.php";
$app = new \Slim\App($settings);

// Check for configuration problems
$dotenv->required([
	"USERNAME",
	"PASSWORD",
	"SECRET",
	"UPLOAD_DIR",
	"DB_CONNECTION"
])->notEmpty();

// Set up dependencies
require __DIR__ . "/dependencies.php";

// Register routes
require __DIR__ . "/routes.php";

// Register models
require __DIR__ . "/models.php";

// Register authentication middleware
require __DIR__ . "/authentication.php";
