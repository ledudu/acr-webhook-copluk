<?php namespace ACR;

/**
 * @Entity
 * @Table(name="recordings")
 */
class Recording {

	/**
	 * @Id
	 * @Column(type="integer")
	 * @GeneratedValue
	 */
	public $id;

	/**
	 * @Column(type="string", name="file_name", nullable=false)
	 */
	public $fileName;

	/**
	 * @Column(type="datetime", options={"comment": "Unix timestamp of the recording"})
	 */
	public $date;

	/**
	 * @Column(type="integer", options={"comment": "Call duration, in milliseconds"})
	 */
	public $duration;

	/**
	 * @Column(type="smallint", options={"comment": "0 or 1"})
	 */
	public $direction;

	/**
	 * @Column(type="boolean")
	 */
	public $important;

	/**
	 * @Column(type="string")
	 */
	public $phone;

	/**
	 * @Column(type="string")
	 */
	public $contact;

	/**
	 * @Column(type="string")
	 */
	public $note;

}
